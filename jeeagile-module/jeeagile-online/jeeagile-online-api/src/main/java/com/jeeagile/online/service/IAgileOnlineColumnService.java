package com.jeeagile.online.service;

import com.jeeagile.online.entity.AgileOnlineColumn;
import com.jeeagile.frame.service.IAgileBaseService;

/**
 * @author JeeAgile
 * @date 2023-07-27
 * @description 在线表单 表单数据表字段 接口
 */
public interface IAgileOnlineColumnService extends IAgileBaseService<AgileOnlineColumn> {

}
