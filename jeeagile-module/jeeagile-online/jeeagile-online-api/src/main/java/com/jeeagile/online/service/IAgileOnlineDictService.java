package com.jeeagile.online.service;

import com.jeeagile.online.entity.AgileOnlineDict;
import com.jeeagile.frame.service.IAgileBaseService;

/**
 * @author JeeAgile
 * @date 2023-07-13
 * @description 在线表单 字典管理 接口
 */
public interface IAgileOnlineDictService extends IAgileBaseService<AgileOnlineDict> {

}
