package com.jeeagile.online.service;

import com.jeeagile.frame.service.IAgileBaseService;
import com.jeeagile.online.entity.AgileOnlineRule;

/**
 * @author JeeAgile
 * @date 2023-08-21
 * @description 在线表单 规则配置 接口
 */
public interface IAgileOnlineRuleService extends IAgileBaseService<AgileOnlineRule> {

}
