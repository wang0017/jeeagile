package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.online.entity.AgileOnlineTable;
import com.jeeagile.frame.mapper.AgileBaseMapper;

/**
 * @author JeeAgile
 * @date 2023-07-25
 * @description 在线表单 数据模型 数据库操作接口
 */
@AgileMapper
public interface AgileOnlineTableMapper extends AgileBaseMapper<AgileOnlineTable> {

}
