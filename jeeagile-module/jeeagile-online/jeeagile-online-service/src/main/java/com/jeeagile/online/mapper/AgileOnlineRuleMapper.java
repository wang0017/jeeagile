package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.frame.mapper.AgileBaseMapper;
import com.jeeagile.online.entity.AgileOnlineRule;

/**
 * @author JeeAgile
 * @date 2023-08-21
 * @description 在线表单 规则配置 数据库操作接口
 */
@AgileMapper
public interface AgileOnlineRuleMapper extends AgileBaseMapper<AgileOnlineRule> {

}
