package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.online.entity.AgileOnlineColumn;
import com.jeeagile.frame.mapper.AgileBaseMapper;

/**
 * @author JeeAgile
 * @date 2023-07-27
 * @description 在线表单 数据表字段 数据库操作接口
 */
@AgileMapper
public interface AgileOnlineColumnMapper extends AgileBaseMapper<AgileOnlineColumn> {

}
