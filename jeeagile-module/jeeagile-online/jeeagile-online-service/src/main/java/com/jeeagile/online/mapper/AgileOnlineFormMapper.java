package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.online.entity.AgileOnlineForm;
import com.jeeagile.frame.mapper.AgileBaseMapper;

/**
 * @author JeeAgile
 * @date 2023-07-19
 * @description 在线表单 表单信息 数据库操作接口
 */
@AgileMapper
public interface AgileOnlineFormMapper extends AgileBaseMapper<AgileOnlineForm> {

}
