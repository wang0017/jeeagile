package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.online.entity.AgileOnlineDict;
import com.jeeagile.frame.mapper.AgileBaseMapper;

/**
 * @author JeeAgile
 * @date 2023-07-13
 * @description 在线表单 字典管理 数据库操作接口
 */
@AgileMapper
public interface AgileOnlineDictMapper extends AgileBaseMapper<AgileOnlineDict> {

}
