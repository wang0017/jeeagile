package com.jeeagile.online.mapper;

import com.jeeagile.frame.annotation.AgileMapper;
import com.jeeagile.online.entity.AgileOnlinePage;
import com.jeeagile.frame.mapper.AgileBaseMapper;

/**
 * @author JeeAgile
 * @date 2023-07-31
 * @description 在线表单 表单页面 数据库操作接口
 */
@AgileMapper
public interface AgileOnlinePageMapper extends AgileBaseMapper<AgileOnlinePage> {

}
