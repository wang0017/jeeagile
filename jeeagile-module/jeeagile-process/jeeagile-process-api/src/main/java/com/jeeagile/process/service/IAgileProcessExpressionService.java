package com.jeeagile.process.service;

import com.jeeagile.frame.service.IAgileBaseService;
import com.jeeagile.process.entity.AgileProcessExpression;

/**
 * @author JeeAgile
 * @date 2022-08-08
 * @description 流程表达式
 */
public interface IAgileProcessExpressionService extends IAgileBaseService<AgileProcessExpression> {
}
